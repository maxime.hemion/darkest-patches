# Darkest patches

Provides patches for Darkest Dungeon community mods

All patches should include mod version from which they were created.
Because there is no way to tell what could be the version number / commit sha or whatever from the upstream mod
the best approach to get a version is doing:
`ls -al DIRECTORY`
and finding the latest changes datetime, which will be considered "version"
...
What a wonderful world
